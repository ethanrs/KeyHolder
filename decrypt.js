/*
	Ethan Semella
	Keyholder
	January 2017
*/



$(document).ready(function() {
	var info;
	var url = getDomain(window.location.href);
	// load password info for this url

	chrome.storage.local.get(null, function(e) {
		info = e[url];
	});

	// load password info for this url when password input field changed
	$('input[type="password"]').change(function() {
		chrome.storage.local.get(null, function(e) {
			info = e[url];
		});
	});

	// replace password input field with corresponding decrypted password
	$('input[type="submit"], button[type="submit"]').click(function() {
		var temp = $('input[type="password"]').val();
		var pass = decrypt(temp, info.cipher, info.MAC, info.salt, info.IV);
		$('input[type="password"]').val(pass);
	});
});

// same as getDomain in view.js
function getDomain(url) {
	var domain;
    if (url.indexOf("://") > -1) {
		domain = url.split('/')[2];
	} else {
		domain = url.split('/')[0];
	}
	if (domain.indexOf("www.") > -1) {
		var temp = domain.split('.');
		domain = temp[1];
		for (var i = 2; i < temp.length; i++) {
			domain = domain + "." + temp[i];
		}
	}
	domain = domain.split(':')[0];
	return domain;
}


/*
	All below functions and declarations same as encrypt.js with the exception of
	the decrypt function.
*/


var k = [0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
		0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
		0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
		0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
		0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
		0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2];

var iv = [0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9a, 0x5be0cd19];

for (var i = 0; i < 64; i++) {
	k[i] = k[i].toString(2);
	k[i] = k[i].split("");
	for (var j = 0; j < k[i].length; j++) {
		k[i][j] = parseInt(k[i][j]);
	}
	k[i] = zerofill(k[i], 32);
}
for (var i = 0; i < iv.length; i++) {
	iv[i] = iv[i].toString(2);
	iv[i] = iv[i].split("");
	for (var j = 0; j < iv[i].length; j++) {
		iv[i][j] = parseInt(iv[i][j]);
	}
	iv[i] = zerofill(iv[i], 32);
}

var sbox = [99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118,
			202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192,
			183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21,
			4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117,
			9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132,
			83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207,
			208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168,
			81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210,
			205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115,
			96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219,
			224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121,
			231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8,
			186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138,
			112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158,
			225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223,
			140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22];

var rcon = [[0, 0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 1, 0, 0],
			[0, 0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0, 0],
			[0, 1, 0, 0, 0, 0, 0, 0], [1, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 1, 0, 1, 1],
			[0, 0, 1, 1, 0, 1, 1, 0], [0, 1, 1, 0, 1, 1, 0, 0], [1, 1, 0, 1, 1, 0, 0, 0],
			[1, 0, 1, 0, 1, 0, 1, 1], [0, 1, 0, 0, 1, 1, 0, 1], [1, 0, 0, 1, 1, 0, 1, 0]];


function decrypt(master, cipher, MAC, salt, cfbiv) {
	// convert master from string to bit array
	var temp = "";
	for (var i = 0; i < master.length; i++) {
		temp = temp + master.charCodeAt(i).toString(2);
	}
	master = temp.split("");
	for (var i = 0; i < master.length; i++) {
		master[i] = parseInt(master[i]);
	}

	// stretch key
	var tempKey = PBKDF2(master, salt, 128, 256);
	var aKey = PBKDF2(tempKey, cfbiv, 1, 512);
	var eKey = aKey.splice(0, 256);



	// authenticate ciphertext
	var temp = HMAC(aKey, cipher);
	for (var i = 0; i < temp.length; i++) {
		if (MAC[i] != temp[i]) {
			return;
		} 
	}

	// decrypt ciphertext
	var plain = CFBDecrypt(cfbiv, eKey, cipher);

	// convert plaintext from bit array to string
	var decrypted = "";
	for (var i = 0; i < plain.length; i += 8) {
		var letter = plain.slice(i, i + 8);
		var value = 0;
		var place = 1;
		for (var j = 7; j >= 0; j--) {
			if (letter[j] == 1) value += place;
			place *= 2;
		}
		if (value != 0) decrypted += String.fromCharCode(value);
	}
	return decrypted;
}

function rightRotate(num, bits) {
	for (var i = 0; i < bits; i++) {
		var temp = num[num.length - 1];
		num.pop();
		num.unshift(temp);
	}	
	return num;
}

function xor(a, b) {
	if (a.length < b.length) {
		a = zerofill(a, b.length);
	} else if (b.length < a.length) {
		b = zerofill(b, a.length);
	}
	var c = [];
	for (var i = 0; i < a.length; i++) {
		if (a[i] == b[i]) {
			c.push(0);
		} else {
			c.push(1);
		}
	}

	return c;
}

function zerofill(num, length) {
	while (num.length < length) {
		num.unshift(0);
	}
	return num;
}

function add(a, b) {
	if (a.length < b.length) {
		a = zerofill(a, b.length);
	} else if (b.length < a.length) {
		b = zerofill(b, a.length);
	}

	var c = new Array(a.length).fill(0);
	var carry = 0;
	for (var i = a.length - 1; i >= 0; i--) {
		var sum = a[i] + b[i] + carry;
		if (sum == 3) {
			carry = 1;
			c[i] = 1;
		} else if (sum == 2) {
			carry = 1;
			c[i] = 0;
		} else if (sum == 1) {
			carry = 0;
			c[i] = 1;
		} else {
			carry = 0;
			c[i] = 0;
		}
	}

	return c;
}

function not(a) {
	for (var i = 0; i < a.length; i++) {
		if (a[i] == 0) {
			a[i] = 1;
		} else {
			a[i] = 0;
		}
	}
	return a;
}

function and(a, b) {
	if (a.length < b.length) {
		a = zerofill(a, b.length);
	} else if (b.length < a.length) {
		b = zerofill(b, a.length);
	}

	var c = [];
	for (var i = 0; i < a.length; i++) {
		if (a[i] == 1 && b[i] == 1) {
			c[i] = 1;
		} else {
			c[i] = 0;
		}
	}

	return c;
}

function SHA256(message) {
	var h0 = iv[0].slice();
	var h1 = iv[1].slice();
	var h2 = iv[2].slice();
	var h3 = iv[3].slice();
	var h4 = iv[4].slice();
	var h5 = iv[5].slice();
	var h6 = iv[6].slice();
	var h7 = iv[7].slice();

	var bitlength = message.length.toString(2);
	bitlength = bitlength.split("");
	for (var i = 0; i < bitlength.length; i++) {
		bitlength[i] = parseInt(bitlength[i]);
	}

	message.push(1);
	var templength = message.length;
	while (templength % 512 != 448) {
		message.push(0);
		templength = message.length;
	}

	tempLength = bitlength.length;
	while (templength != 64) {
		bitlength.push(0);
		templength = bitlength.length;
	}
	message = message.concat(bitlength);

	var chunks = [];
	while (message.length > 0) {
		chunks.push(message.splice(0, 512));
	}

	for (var i = 0; i < chunks.length; i++) {
		var w = [];
		while (chunks[i].length > 0) {
			w.push(chunks[i].splice(0, 32));
		}
		for (var i = w.length; i < 64; i++) w.push([]);

		for (var j = 16; j < 64; j++) {
			var s0 = xor(xor(rightRotate(w[j - 15], 7), rightRotate(w[j - 15], 18)),
						rightRotate(w[j - 15], 3));
			var s1 = xor(xor(rightRotate(w[j - 2], 17), rightRotate(w[j - 2], 19)),
						rightRotate(w[j - 2], 10));
			s0 = zerofill(s0, 32);
			s1 = zerofill(s1, 32);
			w[j - 16] = zerofill(w[j - 16], 32);
			w[j - 7] = zerofill(w[j - 7], 32);
			w[j] = add(add(add(w[j - 16], s0), w[j - 7]), s1);
			w[j] = zerofill(w[j], 32);
		}

		var a = h0;
		var b = h1;
		var c = h2;
		var d = h3;
		var e = h4;
		var f = h5;
		var g = h6;
		var h = h7;

		for (var j = 0; j < 64; j++) {
			var s1 = xor(xor(rightRotate(e, 6), rightRotate(e, 11)), rightRotate(e, 25));
			s1 = zerofill(s1, 32);
			var ch = xor(and(e, f), and(not(e), g));
			ch = zerofill(ch, 32);
			var temp1 = add(add(add(add(h, s1), ch), k[j].slice()), w[j]);
			temp1 = zerofill(temp1, 32);
			var s0 = xor(xor(rightRotate(a, 2), rightRotate(a, 13)), rightRotate(a, 22));
			s0 = zerofill(s0, 32);
			var maj = xor(xor(and(a, b), and(a, c)), and(b, c));
			maj = zerofill(maj, 32);
			var temp2 = add(s0, maj);
			temp2 = zerofill(temp2, 32);

			h = g;
			g = f;
			f = e;
			e = add(d, temp1);
			e = zerofill(e, 32);
			d = c;
			c = b;
			b = a;
			a = add(temp1, temp2);
			a = zerofill(a, 32);
		}

		h0 = add(h0, a);
		h1 = add(h1, b);
		h2 = add(h2, c);
		h3 = add(h3, d);
		h4 = add(h4, e);
		h5 = add(h5, f);
		h6 = add(h6, g);
		h7 = add(h7, h);
		h0 = zerofill(h0, 32);
		h1 = zerofill(h1, 32);
		h2 = zerofill(h2, 32);
		h3 = zerofill(h3, 32);
		h4 = zerofill(h4, 32);
		h5 = zerofill(h5, 32);
		h6 = zerofill(h6, 32);
		h7 = zerofill(h7, 32);
	}
	return h0.concat(h1).concat(h2).concat(h3).concat(h4).concat(h5).concat(h6).concat(h7);
}

function HMAC(key, message) {
	var ipad = (0x36 * 256).toString(2);
	ipad = ipad.split("");
	for (var i = 0; i < ipad.length; i++) {
		ipad[i] = parseInt(ipad[i]);
	}

	var opad = (0x5c * 256).toString(2);
	opad = opad.split("");
	for (var i = 0; i < opad.length; i++) {
		opad[i] = parseInt(opad[i]);
	}

	message = SHA256(xor(key, ipad).concat(message));
	return SHA256(xor(key, opad).concat(message));
}

function PBKDF2(key, salt, iterations, length) {
	var t = [];
	for (var i = 0; i < length / 256; i++) {
		var u = [];
		var index = [];
		var place = 128;
		var value = i;
		for (var j = 0; j < 8; j++) {
			if (value >= place) {
				value -= place;
				index[j] = 1;
			} else {
				index[j] = 0;
			}
			place /= 2;
		}
		u[0] = HMAC(key, salt.concat(zerofill(index, 32)));
		t[i] = u[0];
		for (var j = 1; j < iterations; j++) {
			u[j] = HMAC(key, u[j - 1]);
			t[i] = xor(t[i], u[j]);
		}
	}
	var retVal = t[0];
	for (var i = 1; i < length / 256; i++) {
		retVal = retVal.concat(t[i]);
	}
	return retVal;
}

function keyScheduleCore(key, round) {
	for (var i = 0; i < 8; i++) {
		for (var j = 1; j < 32; j++) {
			var temp = key[j];
			key[j] = key[j - 1];
			key[j - 1] = temp;
		}
	}
	for (var i = 0; i < 4; i++) {
		var value = 0;
		var place = 1;
		for (var j = 7; j >= 0; j--) {
			if (key[j + (i * 8)] == 1) value += place;
			place *= 2;
		}
		value = sbox[value];
		place = 128;
		for (var j = 0; j < 8; j++) {
			if (value >= place) {
				value -= place;
				key[j + (i * 8)] = 1;
			} else {
				key[j + (i * 8)] = 0;
			}
			place /= 2;
		}
	}
	var lastByte = [];
	for (var i = 0; i < 8; i++) {
		lastByte[i] = key[i];
	}
	lastByte = xor(lastByte, rcon[round]);
	for (var i = 0; i < 8; i++) {
		key[i] = lastByte[i];
	}
	return key;
}

function keySchedule(key) {
	var keys = [];
	for (var i = 0; i < 8; i++) {
		keys[i] = [];
		for (var j = 0; j < 32; j++) {
			keys[i][j] = key[j + (i * 32)];
		}
	}
	var round = 0;
	while (keys.length < 60) {
		var old = keys.length;
		var prev = keyScheduleCore(keys[old - 1], round);
		for (var i = 0; i < old + 4; i++) {
			keys[old + i] = [];
			keys[old + i] = xor(prev, keys[old + i - 4]);
			prev = keys[old + i];
		}
		round++;
	}
	var roundKeys = [];
	for (var i = 0; i < 15; i++) {
		roundKeys[i] = [];
		for (var j = 0; j < 4; j++) {
			for (var l = 0; l < 32; l++) {
				roundKeys[i][l + (j * 32)] = keys[j + (i * 4)][l];
			}
		}
	}
	return roundKeys;
}

function subBytes(block) {
	for (var i = 0; i < 128; i += 8) {
		var sub = [];
		var value = 0;
		var place = 1;
		for (var j = 7; j >= 0; j--) {
			if (block[i + j] == 1) value += place;
			place *= 2;
		}
		value = sbox[value];
		place = 128;
		for (var j = 0; j < 8; j++) {
			if (value >= place) {
				value -= place;
				block[i + j] = 1;
			} else {
				block[i + j] = 0;
			}
			place /= 2;
		}
	}
	return block;
}

function shiftRows(block) {
	var temp = [];
	for (var i = 32; i < 56; i += 8) {
		for (var j = 0; j < 8; j++) {
			temp[j] = block[i + j];
			block[i + j] = block[i + j + 8];
			block[i + j + 8] = temp[j];
		}
	}
	for (var l = 0; l < 2; l++) {
		for (var i = 64; i < 88; i += 8) {
			for (var j = 0; j < 8; j++) {
				temp[j] = block[i + j];
				block[i + j] = block[i + j + 8];
				block[i + j + 8] = temp[j];
			}
		}
	}
	for (var l = 0; l < 3; l++) {
		for (var i = 96; i < 120; i += 8) {
			for (var j = 0; j < 8; j++) {
				temp[j] = block[i + j];
				block[i + j] = block[i + j + 8];
				block[i + j + 8] = temp[j];
			}
		}
	}
	return block;
}

function mult(arr, scalar) {
	var ret = [];
	for (var i = 0; i < 8; i++) {
		ret[i] = arr[i];
	}
	if (scalar == 2) {
		if (ret[0] == 0) {
			for (var i = 0; i < 7; i++) {
				var temp = ret[i];
				ret[i] = ret[i + 1];
				ret[i + 1] = temp;
			}
		} else if (ret[0] == 1) {
			for (var i = 0; i < 7; i++) {
				var temp = ret[i];
				ret[i] = ret[i + 1];
				ret[i + 1] = temp;
			}
			ret[7] = 0;
			var c = [0, 0, 0, 1, 1, 0, 1, 1];
			ret = xor(ret, c);
		}
		return ret;
	} else if (scalar == 3) {
		return xor(mult(ret, 2), ret);
	}
}

function mixColumns(block) {
	for (var i = 0; i < 32; i += 8) {
		var a = [];
		for (var j = i; j < i + 128; j += 32) {
			var elem = [];
			for (var l = 0; l < 8; l++) {
				elem[l] = block[l + j];
			}
			a.push(elem);
		}
		var b = [];
		b[0] = xor(xor(xor(mult(a[0], 2), mult(a[1], 3)), a[2]), a[3]);
		b[1] = xor(xor(xor(a[0], mult(a[1], 2)), mult(a[2], 3)), a[3]);
		b[2] = xor(xor(xor(a[0], a[1]), mult(a[2], 2)), mult(a[3], 3));
		b[3] = xor(xor(xor(mult(a[0], 3), a[1]), a[2]), mult(a[3], 2));

		for (var j = 0; j < 4; j++) {
			for (var l = 0; l < 8; l++) {
				block[l + i + (j * 32)] = b[j][l];
			}
		}
	}
	return block;
}

function AES256(key, plaintext) {
	var keys = keySchedule(key);
	var ciphertext = plaintext;
	ciphertext = xor(ciphertext, keys[0]);
	for (var i = 0; i < keys.length - 1; i++) {
		ciphertext = subBytes(ciphertext);
		ciphertext = shiftRows(ciphertext);
		ciphertext = mixColumns(ciphertext);
		ciphertext = xor(ciphertext, keys[i]);
	}
	ciphertext = subBytes(ciphertext);
	ciphertext = shiftRows(ciphertext);
	ciphertext = xor(ciphertext, keys[keys.length - 1]);
	return ciphertext;
}

function CFBDecrypt(cfbiv, key, ciphertext) {
	var blocks = [];
	blocks[0] = cfbiv;
	while (ciphertext.length > 0) {
		blocks.push(ciphertext.splice(0, 128));
	}
	plaintext = [];	
	for (var i = 1; i < blocks.length; i++) {
		plaintext[i - 1] = xor(AES256(key, blocks[i - 1]), blocks[i]);
	}
	var retVal = [].concat.apply([], plaintext);
	return retVal;
}
